the lego keyboard
=================

This is a small and cleaned version of the original repo.

We are using Raspberry Pico.

v5 is current and working (rev4).
rev5/rev6 - rp2040 support, rpico, weact and other compatible clones.

motivation
----------

We did this as a team building workshop. Here you only find our learnings. 
See original repository for complete information and current state.

The idea was to have an iso friendly ortholinear keyboard.
This a 5x13 option, more info in [here](https://alin.elena.space/blog/keeblego/)

what is special about this one?

  * the case is set in lego (almost lego)
  * microcontroller is just a standard Raspberry Pico
  * rotary encoder (no switch)

**note** the holes in current version are cut at the official lego dimension. Unfortunately the drilling machines have tolerance and
the same is true for the lego making tools... the tolerance in lego varies with colour of the plastic... you will find some colours may not fit
as well as others. I suggest you to use 1x2 and 1x4 plates if you want the finest keyboard, since they tend to bend the pcb... you will be
amased but the force.

Bill of Materials (BoM)
---

Piece   Number of pieces per Keyboard
- [1x2](https://www.brickowl.de/catalog/lego-plate-1-x-2-3023-6225) lego 46	
- [plate 16x32 studs](https://www.brickowl.de/catalog/lego-baseplate-16-x-32-2748)		1	
- 4 1x1 lego tiles or eyes.(optionally)		4	https://www.brickowl.de/catalog/lego-plate-1-x-1-3024-30008
- optional 4 2x2 corner plates(optionally)		4	https://www.brickowl.de/catalog/lego-plate-2-x-2-corner-2420-63325
- Lego gesamt			
- signal diodes 1N4148		65	https://www.reichelt.de/schalt-diode-100-v-150-ma-do-35-1n-4148-p1730.html?&trstct=pos_0&nbc=1
- 2 resistors (510Ω (R1,R2)		2	https://www.reichelt.de/duennschichtwiderstand-axial-0-6-w-510-ohm-1--vi-mbb02070c5100-p233748.html?&trstct=pos_5&nbc=1
- Raspberry Pi Pico --> wir brauchen diesen, weil wir REV5 des Boards bestellt haben		1	https://www.berrybase.de/raspberry-pi-pico-rp2040-mikrocontroller-board-mit-headern
- Raspberry Pi Pico Klon mit USB-C		1	https://de.aliexpress.com/item/1005004264082770.html
- 100kΩ resistor for (R3)		1	https://www.reichelt.de/widerstand-metallschicht-100-kohm-0207-0-6-w-1--metall-100k-p11458.html?&trstct=pos_0&nbc=1
- switches (5 pin) and keycaps... for pcb mount		65	https://www.mouser.de/account/orders/detail?qs=9C%2fvjDbyW3Vp27quMq%2fydoaYxxzHf0gR%252BFIUpH2IDXw%3d
- rotary encoder (I got this Bourns 24 Pulse Incremental Mechanical Rotary Encoder with a 6 mm Flat Shaft but any similar shall do)		1	https://www.reichelt.de/drehimpulsegeber-24-impulse-24-rastungen-vertikal-stec12e08-p73923.html?&trstct=pos_1&nbc=1
- Sockel für MCU		1	https://www.reichelt.de/ic-sockel-40-polig-doppelter-federkontakt-gs-40-p8224.html?&trstct=pos_1&nbc=1
- Micro-USB-Kabel		0	
- PCB		1	https://gitlab.com/m-lego/m65
- caps		65	https://ymdkey.com/652279869/orders/230159120927326e92e103226ceff25c
- Aluminium Platten		1	https://www.alu-verkauf.de/Aluminium-Aluminiumprofile/Platten

We placed aluminium plates below the PCB to have a solid foundation

assembly
--------

this is a very rush [assembly guide](assembly.md) but shall give you the main idea.

pictures
--------

  [v4](pics/m65-v4.png)
  [the layout](pics/m65-layout.png)
  [Rev 4](pics/m65-rev4.png)
  [Rev 4 PCB](pics/m65-rev4-pcb.png)

firmware
--------

   layout is missing

   microcontroller flashing:
#TODO

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make mlego/m65/rev1:uk
   make mlego/m65/rev1:uk:flash
```

you can use also gdf303 from we act aka bluepill plus  https://github.com/WeActTC/BluePill-Plus

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make mlego/m65/rev2:uk
   make mlego/m65/rev2:uk:flash
```

you can use also stm32f401 from we act

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make mlego/m65/rev3:uk
   make mlego/m65/rev3:uk:flash
```

for rev4

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make mlego/m65/rev4:uk
   make mlego/m65/rev4:uk:flash
```


pins and more
=============

## Pins and leds rev1 STM/APM32F103C8T6

similar pinout for STM32F303

| Rows  | C0    | C1    | C2    | C3    | C4    | C5    | C6    | C7    | C8    | C9    | C10   | C11   | C12   | Pins  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| R0    | Esc   | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     | 9     | 0     | -     | Bksp  | B11   |
| R1    | Tab   | q     | w     | e     | r     | t     | y     | u     | i     | o     | p     | [     | ]     | B0    |
| R2    | #     | a     | s     | d     | f     | g     | h     | j     | k     | l     | ;     | '     | Enter | B1    |
| R3    | Shift | \     | z     | x     | c     | v     | b     | n     | m     | ,     | .     | Up    | /     | A2    |
| R4    | Ctrl  | Menu  | Lower | Alt   | Raise | Space | Space | Space | AltGr | Shift | Left  | Down  | Right | A3    |
|       | A10   | A15   | B3    | B4    | B5    | B9    | B8    | B7    | B6    | C15   | A0    | A7    | B10   |       |

### Encoders

  - Pad_A: A8
  - Pad_B: A9

### Leds

| Leds        | Pin |
| ----------- | --- |
| NUM_LOCK    | B12 |
| CAPS_LOCK   | C13 |
| SCROLL_LOCK | B13 |
| RBG_DI      | B15 |

## Pins and leds rev2 GD32F303CCT6


| Rows  | C0    | C1    | C2    | C3    | C4    | C5    | C6    | C7    | C8    | C9    | C10   | C11   | C12   | Pins  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| R0    | Esc   | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     | 9     | 0     | -     | Bksp  | B11   |
| R1    | Tab   | q     | w     | e     | r     | t     | y     | u     | i     | o     | p     | [     | ]     | B0    |
| R2    | #     | a     | s     | d     | f     | g     | h     | j     | k     | l     | ;     | '     | Enter | B1    |
| R3    | Shift | \     | z     | x     | c     | v     | b     | n     | m     | ,     | .     | Up    | /     | A2    |
| R4    | Ctrl  | Menu  | Lower | Alt   | Raise | Space | Space | Space | AltGr | Shift | Left  | Down  | Right | A3    |
|       | A10   | A15   | B3    | B4    | B5    | B9    | B8    | B7    | B6    | C15   | A0    | A7    | B10   |       |


### Encoders

  - Pad_A: A8
  - Pad_B: A9

### Leds

| Leds        | Pin |
| ----------- | --- |
| NUM_LOCK    | B12 |
| CAPS_LOCK   | B2  |
| SCROLL_LOCK | B13 |
| RBG_DI      | B15 |


## Pins and leds rev3 stm32f401

the pinout is the same for stm32f411

| Rows  | C0    | C1    | C2    | C3    | C4    | C5    | C6    | C7    | C8    | C9    | C10   | C11   | C12   | Pins  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| R0    | Esc   | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     | 9     | 0     | -     | Bksp  | B10   |
| R1    | Tab   | q     | w     | e     | r     | t     | y     | u     | i     | o     | p     | [     | ]     | A5    |
| R2    | #     | a     | s     | d     | f     | g     | h     | j     | k     | l     | ;     | '     | Enter | A6    |
| R3    | Shift | \     | z     | x     | c     | v     | b     | n     | m     | ,     | .     | Up    | /     | A7    |
| R4    | Ctrl  | Menu  | Lower | Alt   | Raise | Space | Space | Space | AltGr | Shift | Left  | Down  | Right | B0    |
|       | A10   | A15   | B3    | B4    | B5    | B9    | B8    | B7    | A1    | A2    | A3    | A4    | B1    |       |

### Encoders

  - Pad_A: A0
  - Pad_B: B6

### LEDS

| Leds        | Pin |
| ----------- | --- |
| NUM_LOCK    | B12 |
| CAPS_LOCK   | C13 |
| SCROLL_LOCK | B13 |
| RBG_DI      | B15 |

## Pins and leds rev4 stm32f401

the pinout is the same for stm32f411

| Rows  | C0    | C1    | C2    | C3    | C4    | C5    | C6    | C7    | C8    | C9    | C10   | C11   | C12   | Pins  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| R0    | Esc   | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     | 9     | 0     | -     | Bksp  | B10   |
| R1    | Tab   | q     | w     | e     | r     | t     | y     | u     | i     | o     | p     | [     | ]     | A5    |
| R2    | #     | a     | s     | d     | f     | g     | h     | j     | k     | l     | ;     | '     | Enter | A6    |
| R3    | Shift | \     | z     | x     | c     | v     | b     | n     | m     | ,     | .     | Up    | /     | A7    |
| R4    | Ctrl  | Menu  | Lower | Alt   | Raise | Space | Space | Space | AltGr | Shift | Left  | Down  | Right | B0    |
|       | B14   | A8    | A10   | A15   | B3    | B4    | B5    | B7    | A1    | A2    | A3    | A4    | B1    |       |

### Encoders

  - Pad_A: A0
  - Pad_B: B6

### Oled

  - SDA: B9
  - SCL/SCK: B8

### LEDS

| Leds        | Pin |
| ----------- | --- |
| NUM_LOCK    | B12 |
| CAPS_LOCK   | C13 |
| SCROLL_LOCK | B13 |
| RBG_DI      | B15 |

## Pins and leds rev5 rp2040 Raspberry Pico, Teenstar

| Rows  | C0    | C1    | C2    | C3    | C4    | C5    | C6    | C7    | C8    | C9    | C10   | C11   | C12   | Pins  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| R0    | Esc   | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     | 9     | 0     | -     | Bksp  | GP22  |
| R1    | Tab   | q     | w     | e     | r     | t     | y     | u     | i     | o     | p     | [     | ]     | GP16  |
| R2    | #     | a     | s     | d     | f     | g     | h     | j     | k     | l     | ;     | '     | Enter | GP18  |
| R3    | Shift | \     | z     | x     | c     | v     | b     | n     | m     | ,     | .     | Up    | /     | GP19  |
| R4    | Ctrl  | Menu  | Lower | Alt   | Raise | Space | Space | Space | AltGr | Shift | Left  | Down  | Right | GP20  |
|       | GP1   | GP6   | GP7   | GP8   | GP9   | GP15  | GP14  | GP13  | GP12  | GP11  | GP10  | GP17  | GP21  |       |

### Encoders

  - Pad_A: GP4
  - Pad_B: GP5

### Oled

  - SDA: GP2
  - SCL/SCK: GP3

### LEDS

| Leds        | Pin |
| ----------- | --- |
| NUM_LOCK    | GP28 |
| CAPS_LOCK   | GP25 |
| SCROLL_LOCK | GP27 |
| RBG_DI      | GP0 |


## Pins and leds rev6 rp2040 weact

| Rows  | C0    | C1    | C2    | C3    | C4    | C5    | C6    | C7    | C8    | C9    | C10   | C11   | C12   | Pins  |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| R0    | Esc   | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     | 9     | 0     | -     | Bksp  | GP22  |
| R1    | Tab   | q     | w     | e     | r     | t     | y     | u     | i     | o     | p     | [     | ]     | GP16  |
| R2    | #     | a     | s     | d     | f     | g     | h     | j     | k     | l     | ;     | '     | Enter | GP18  |
| R3    | Shift | \     | z     | x     | c     | v     | b     | n     | m     | ,     | .     | Up    | /     | GP19  |
| R4    | Ctrl  | Menu  | Lower | Alt   | Raise | Space | Space | Space | AltGr | Shift | Left  | Down  | Right | GP20  |
|       | GP1   | GP6   | GP7   | GP8   | GP9   | GP15  | GP14  | GP13  | GP12  | GP11  | GP10  | GP17  | GP21  |       |

### Encoders

  - Pad_A: GP4
  - Pad_B: GP5

### Oled

  - SDA: GP2
  - SCL/SCK: GP3

### LEDS

| Leds        | Pin |
| ----------- | --- |
| NUM_LOCK    | GP29 |
| CAPS_LOCK   | GP25 |
| SCROLL_LOCK | GP28 |
| RBG_DI      | GP0  |

* while some pin numbers for leds are different physical positions are the same for both rev5 and rev6
