### handwire

handwire is great but is boring.
This is a small 5x5 panel of that supports both MX and chocov2.
needs to be cut to write dimensions.

#### render

  ![handwire 5x5](pics/hand/hand-5x5-3d.png)


#### pcb

  ![handwire 5x5](pics/hand/m1-5x5-pcb.png)

  ![handwire 5x5](pics/hand/hand-5x5-3d-back.png)

#### gerbers

ready may gerbers for jlcpcb are available [here](https://gitlab.com/m-lego/hand5x5/-/blob/main/m155-gerbers.zip)

full kicad project is available https://gitlab.com/m-lego/hand5x5 footprints are shared from [m65](https://gitlab.com/m-lego/m65)
