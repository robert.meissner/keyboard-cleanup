## tipro ortho ansi

### introduction

this is an ortho ansi combination from tipro that I have converted to use qmk using same pcb as [here](https://mlego.elena.space/tipro8x16/)

this is in working, page updated once finished.


### wiring

### firmware

### other pictures

original

![m128 ortho ansi ](pics/tipro/tipro-ortho-ansi-original.jpg)

cleaned

![m128 ortho ansi ](pics/tipro/tipro-ortho-ansi-v1.jpg)

switches

![m120 ortho ansi](pics/tipro/tipro-ortho-ansi-v2.jpg)

